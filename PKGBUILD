# Maintainer: Sven Schneider <archlinux.sandmann@googlemail.com>

pkgbase=libfreenect
pkgname=(libfreenect libfreenect-{examples,fakenect,sync,cxx,opencv,python2,openni2})
pkgver=0.5.3
pkgrel=1
pkgdesc="Drivers and libraries for the Xbox Kinect device"
arch=('i686' 'x86_64' 'armv6h' 'armv7h')
url="http://openkinect.org"
license=('GPL2' 'Apache' )
_depends_libfreenect=('libusb')
_depends_examples=('glu' 'freeglut')
_depends_fakenect=('bash')
_depends_sync=()
_depends_cxx=()
_depends_opencv=('opencv')
_depends_python2=('python2')
_depends_openni2=()
makedepends=(
  'cmake'
  'python2-numpy'
  "${_depends_libfreenect[@]}"
  "${_depends_examples[@]}"
  "${_depends_fakenect[@]}"
  "${_depends_sync[@]}"
  "${_depends_cxx[@]}"
  "${_depends_opencv[@]}"
  "${_depends_python2[@]}"
  "${_depends_openni2[@]}")
source=(${pkgbase}-${pkgver}.tar.gz::https://github.com/OpenKinect/libfreenect/archive/v${pkgver}.tar.gz)
sha512sums=('bf5ac23b1ead3dec1d76868373b219a995d648b4da9b317655bc48fc641ec5d8ed5a02228a026c7c95d3c96cc5d5184152be3e5789959e23a332e38167cf7018')

build() {
  cd "${srcdir}/${pkgbase}-${pkgver}"

  # Python 2
  sed -i 's|#!/usr/bin/env python$|&2|' wrappers/python/*.*

  # PYTHON_EXECUTABLE is set to just "python2", but with our version
  # of CMake, it must be an absolute path.  Further, it must be scoped
  # more globally than it is in the CMakeLists.txt, so we define it
  # with -D below.
  sed '/set(PYTHON_EXECUTABLE "python2")/d' -i CMakeLists.txt

  cmake -DCMAKE_INSTALL_PREFIX=/usr \
    -DPROJECT_INCLUDE_INSTALL_DIR=/usr/include \
    -DPYTHON_EXECUTABLE=/usr/bin/python2 \
    -DBUILD_REDIST_PACKAGE=ON \
    -DBUILD_CV=ON \
    -DBUILD_PYTHON=ON \
    -DBUILD_OPENNI2_DRIVER=ON \
    .

  make
}

package_libfreenect() {
  pkgdesc="Library for the Xbox Kinect device"
  depends=("${_depends_libfreenect[@]}")
  optdepends=('python2: for fwfetcher.py script')

  cd "${srcdir}/${pkgbase}-${pkgver}/src"
  make DESTDIR="${pkgdir}" install

  install -Dm644 ../platform/linux/udev/51-kinect.rules "${pkgdir}/usr/lib/udev/rules.d/51-kinect.rules"
  install -d                            "${pkgdir}"/usr/share/libfreenect/
  mv "${pkgdir}"/usr/share/fwfetcher.py "${pkgdir}"/usr/share/libfreenect/
}

package_libfreenect-examples() {
  pkgdesc="Example and utility program for the Xbox Kinect device"
  depends+=("libfreenect-sync=$pkgver" "${_depends_examples[@]}")

  cd "${srcdir}/${pkgbase}-${pkgver}/examples"
  make DESTDIR="${pkgdir}" install
}

package_libfreenect-fakenect() {
  pkgdesc="Mock library for the Xbox Kinect device"
  depends+=("libfreenect=$pkgver" "${_depends_fakenect[@]}")

  cd "${srcdir}/${pkgbase}-${pkgver}/fakenect"
  make DESTDIR="${pkgdir}" install
}

package_libfreenect-sync() {
  pkgdesc="Library for the Xbox Kinect device (synchronous C bindings)"
  depends+=("libfreenect=$pkgver" "${_depends_sync[@]}")

  cd "${srcdir}/${pkgbase}-${pkgver}/wrappers/c_sync"
  make DESTDIR="${pkgdir}" install
}

package_libfreenect-cxx() {
  pkgdesc="Library for the Xbox Kinect device (C++ bindings)"
  depends+=("libfreenect=$pkgver" "${_depends_cxx[@]}")
  optdepends+=(
    'glu: for demos'
    'freeglut: for demos')

  cd "${srcdir}/${pkgbase}-${pkgver}/wrappers/cpp"
  make DESTDIR="${pkgdir}" install
}

package_libfreenect-opencv() {
  pkgdesc="Library for the Xbox Kinect device (OpenCV bindings)"
  depends+=("libfreenect-sync=$pkgver" "${_depends_opencv[@]}")

  cd "${srcdir}/${pkgbase}-${pkgver}/wrappers/opencv"
  make DESTDIR="${pkgdir}" install
}

package_libfreenect-python2() {
  pkgdesc="Library for the Xbox Kinect device (Python 2 bindings)"
  depends+=("libfreenect-sync=$pkgver" "${_depends_python2[@]}")
  optdepends+=(
    'libfreenect-opencv: for demos'
    'python2-matplotlib: for demos')

  cd "${srcdir}/${pkgbase}-${pkgver}/wrappers/python"
  make DESTDIR="${pkgdir}" install

  install -d                     "${pkgdir}"/usr/share/libfreenect/python2-demos
  install -m755 demo_*           "${pkgdir}"/usr/share/libfreenect/python2-demos/
  install -m644 frame_convert.py "${pkgdir}"/usr/share/libfreenect/python2-demos/
}

package_libfreenect-openni2() {
  pkgdesc="Library for the Xbox Kinect device (OpenNI2 bindings)"
  depends+=("libfreenect-cxx=$pkgver" "${_depends_openni2[@]}")

  cd "${srcdir}/${pkgbase}-${pkgver}/OpenNI2-FreenectDriver"
  make DESTDIR="${pkgdir}" install
}
